clear;
clc;
close all;
%%
poolobj = gcp('nocreate');
if isempty(poolobj)
    parpool(6);
end
param.use_gpu = true; % When you set false, the program runs only in Multi-process
param.use_original_guided = false; % When you set false, the program use matlab's guidedfilter. 
                                   % It is much faster, but the result is different from the original one slightly.
%% Loading dataset. 
% We recommend using sub-aperture images from geometric
% calibration toolbox (Bok et al., ECCV 14.)
% The toolbox generates more geometrically correct sub-aperture images than
% other toolboxes.

fname = './dataset/flowers/LF';
load(fname); % input - 5-dimension (t,s,y,x,ch), single type pixel intensities [0,1].

LF = single(LF);
LF = LF./max(LF(:));

%fn_ViewLightField(LF);

%% Cost volume construnction using phase shift in Sec 4.2

%%% default parameters
param.windowsize = [1 1];
param.label = 75;   % number of labels
param.delta = 0.02; % pixel shift unit
param.alpha = 0.5;  % In Eq 5
param.tau1 = 0.5;   % In Eq 6
param.tau2 = 0.5;   % In Eq 8
Sc = [4,4];         % s-t coordinate of reference view
datatype = 2;       % 0: Synthetic Data, 1:Lytro data with long range, 2:Lytro data with short range.

tic;
E1 = double(CostVolume(LF,Sc,param,datatype));
toc;

%% Cost aggregation in Sec 4.2

Ic = im2double(squeeze(LF(Sc(1),Sc(2),:,:,1:3))); % Guided image 

param.r = 5;
param.eps = 0.0001;
tic;
E2 = CostAgg(E1,Ic,param);
toc;

%% Multi-label optimization via Graph-cuts in Sec 4.3

% If there are large holes in the disparity map,
% we recommend using Graph-cuts.
param.data = 2;
param.smooth = 1;
param.neigh = 0.009;
E3 = GraphCuts(E2, Ic, param);
figure; imagesc(E3); axis equal; colorbar;

%%
% If the disparity map is noisy,
% we recommend using weighted median filter.
tic;
E3 = WMT(double(E3), Ic, param);
toc;
figure; imagesc(E3); axis equal; colorbar;

%% Iterative refinement for continuous disparity map in Sec 4.3
param.iternum = 4;
tic;
E4 = IterRefine(E3,Ic,param);
toc;
figure; imagesc(E4); axis equal; colorbar;

%% output
imwrite(squeeze(LF(Sc(1),Sc(2),:,:,1:3)),[fname '_original.png']);
imwrite(E3./param.label,[fname '_label_' num2str(param.label) '_delta_' num2str(param.delta) '_dtype_' num2str(datatype) '_E3.png'])
imwrite(E4./param.label,[fname '_label_' num2str(param.label) '_delta_' num2str(param.delta) '_dtype_' num2str(datatype) '_E4.png'])
