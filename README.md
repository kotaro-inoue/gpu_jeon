## GPU and Multi-process implementation of "Accurate Depth Map Estimation from a Lenslet Light Field Camera, CVPR 2015"
The original code was written by Hae-Gon Jeon and Jaesik Park.  
Note that the code is **ACADEMIC USAGE ONLY** from their license.  

This repository is made by Kotaro Inoue for accelerating their code using GPU and Multi-process.  
They don't know the difference from the original code. 
So if you find some bug, please let me know.

Currently, the code is about 6~7 times faster than the original one.
If you have a faster code, please send me merge requests!

## USAGE
Run MAIN.m

### Recommended Spec
When you use GPU code in Matlab, GPU must have a Compute-Capability 2.0 or higher.  
If you don't have a nice GPU, you can disable to use GPU (please see the MAIN.m).  
CPU: least 4 core  
RAM: least 8GB (recommend:16GB)  
VRAM: least 3GB  

## Bug-fix?
In the refinement, closest(maximum of the label) part will be a clack.
So I modified it like this,  
IterRefine.m, Line: 21
 - Original:  disps = mindisp:step:maxdisp;  
 - Modify: disps = mindisp:step:maxdisp+1;

## Note
This package also includes part of following softwares

- gco-v3.0: Multi-label optimization (http://vision.csd.uwo.ca/code/)
- Fast cost volume filtering: (https://www.ims.tuwien.ac.at/publications/tuw-210567)
- Fast weighted median filter: (http://www.cse.cuhk.edu.hk/~leojia/projects/fastwmedian/index.htm)

---

# How to accelerate?
Mainly, the original code is composed 5 functions (CostVolume, CostAgg, GraphCuts, WMT, and IterRefine).
CostAgg and GraphCuts are already optimized for Matlab. So I modified the others.

## CostVolume
The bottleneck is IFFT.  
So I implemented GPU-IFFT using gpuArray.
Also, I changed single to Multi-process.

## WMT
The bottleneck is "guidedfilter_color_runfilter.m" and "weighted_median_filter.m".

### guidedfilter_color_runfilter.m
I changed scalar calculation to matrix calculation.

### weighted_median_filter.m
I optimized it for Multi-process.

## IterRefine
I optimized it for Multi-process.
Since it also uses guidedfilter_color_runfilter.m, so it much faster than the original one.

## Optional enhancement
### Use Matlab's Guidedfilter
It is much faster than the original guidedfilter_color*.m. but the result is different from the original one slightly.  
Currently, the result may be worse than the original result.  
If you need the best speed, set param.use_original_guided to false in MAIN.m.

---
# Update Log

  - 2018/02/13
    - Support Matlab's guidedfilter (CostAgg.m, WMT.m, and IterRefine)
    - Enhance the performance of SAD and SGD in CostVolume.m
  - 2018/02/11
    - Support GPU and Multi-process

---
# Appendix
## Comparison using Matlab-Profiler
|Original|Optimized(02/13)|
|:-:|:-:|
|![original](https://kotaro-inoue.gitlab.io/img/gpu_jeon/original.png)|![Optimized](https://kotaro-inoue.gitlab.io/img/gpu_jeon/our.png)|
