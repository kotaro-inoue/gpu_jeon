%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2018.2.8 Kotaro Inoue
% Matrix-calculation implementation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function q = guidedfilter_color_runfilter(p,gfobj)
%   guidedfilter_color_runfilter   Run O(1) time implementation of guided filter
%
%   NOTE: you must call guidedfilter_color_precompute first!
%
%   - filtering input image: p (should be a gray-scale/single channel image)

%global gfobj;

%%% For test this funciton %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % First, run the MAIN.m, and then
% r = ceil(max(size(Ic, 1), size(Ic, 2)) / 40);
% eps = 0.01^2;
% gfobj = guidedfilter_color_precompute(Ic, r, eps);
% p=double(E3==20);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[hei, wid] = size(p);

mean_p = boxfilter(p, gfobj.r) ./ gfobj.N;

mean_Ip_r = boxfilter(gfobj.I(:, :, 1).*p, gfobj.r) ./ gfobj.N;
mean_Ip_g = boxfilter(gfobj.I(:, :, 2).*p, gfobj.r) ./ gfobj.N;
mean_Ip_b = boxfilter(gfobj.I(:, :, 3).*p, gfobj.r) ./ gfobj.N;

% covariance of (I, p) in each local patch.
cov_Ip_r = mean_Ip_r - gfobj.mean_I_r .* mean_p;
cov_Ip_g = mean_Ip_g - gfobj.mean_I_g .* mean_p;
cov_Ip_b = mean_Ip_b - gfobj.mean_I_b .* mean_p;

cov_Ip_cell = cell(hei,wid);
for y=1:hei
    for x=1:wid
        cov_Ip_cell{y,x} = [cov_Ip_r(y, x), cov_Ip_g(y, x), cov_Ip_b(y, x)];
    end
end
a = cellfun(@mtimes,cov_Ip_cell,gfobj.invSigma,'UniformOutput',false); % Eqn. (14) in the paper;
a = permute(reshape(cell2mat(a),[hei,3,wid]),[1,3,2]);

b = mean_p - a(:, :, 1) .* gfobj.mean_I_r - a(:, :, 2) .* gfobj.mean_I_g - a(:, :, 3) .* gfobj.mean_I_b; % Eqn. (15) in the paper;

q = (boxfilter(a(:, :, 1), gfobj.r).* gfobj.I(:, :, 1)...
   + boxfilter(a(:, :, 2), gfobj.r).* gfobj.I(:, :, 2)...
   + boxfilter(a(:, :, 3), gfobj.r).* gfobj.I(:, :, 3)...
   + boxfilter(b, gfobj.r)) ./ gfobj.N;  % Eqn. (16) in the paper;
